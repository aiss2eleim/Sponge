#syntax(txtbox text): \n to skip line or use """ ( not nessesary as lineskips are automatic ) 
#                    for colored text use \bCOLOR\bCOLOREDTEXT\b
#                    to move text to next textbox make another string in the list
#                    for multiple choices use a list of the choices instead of strigs
#                    first choice will result in ch0 second in chx (x being #of choices) third chx-1...
class en:
    items = { "i000": ["snails","hailed as \"holy food\" by the cavemen"], #id : [name,description]
              "i001": ["",""] }
    def itmget(self,iid,No):
        return ["You found \b(255,0,0)\b"+str(No)+" "+self.items[iid][0]+"\b"]
    class intro:
        txt = ["""\b(90,170,195)\bGAMBAS TOWN\b,
in the middle of nowhere, out in the sea,
in the very near future.""",
           """A little Sea Slug falls in love with a rock.
He's always been in love with this little stone.
Everyday, he goes in this small abyssal plain
and wastes all his time with this mineral.""",
               "You are this Slug.",
               "Would you like to continue ?"]
        chx = [["yup","nop","maybe"]]
        ch0 = ["good boy"]
        ch1 = ["eh"]
        ch2 = ["k"]
    observations = { "anemone0": ["An anemone\nthis type is a pretty rare one\nit was passed down from your father"
                                 ,"you find it rather cute"],
                     "chair0"  : ["A simple, sturdy chair"],
                     "nontendo": ["This the Newest and Greatest from\nthe video game company \b(255,0,0)\bNontendo®","the \b(255,0,0)\bSUPER NONTENDO\b","Do you want to play ?",["No","No零","No零零"]] }
class fr:
    items = { "i000": ["Escargots","Vénéré comme \"nourriture sacré\" par l'homme des carvernes"], #id : [name,description]
              "i001": ["",""] }
    def itmget(self,iid,No):
        return ["Vous trouvez \b(255,0,0)\b"+str(No)+" "+self.items[iid][0]+"\b"]
    class intro:
        txt = ["""\b(90,170,195)\bGAMBAS TOWN\b,
Au milieu de nullle part, au fond l'océan,
dans un futur très proche.""",
           """Une petite limace de mer tomba amoureuse d'un rocher.
Elle avait toujours été amoureuse de cette petite pierre.
Tout les jours, elle allait dans cette petite plaine abyssale,
pour passer tout son temps avec ce caillou.""",
               "Vous êtes cette limace.",
               "Voulez-vous continuer ?"]
        chx = [["Ouep","nope","Peut-être"]]
        ch0 = ["Bon garçon"]
        ch1 = ["euh"]
        ch2 = ["k"]
    observations = { "anemone0": ["Une anémone\ncette espèce est plutôt rare \nvous l'avez hérité de votre père"
                                 ,"vous la trouvez plutôt mignonne"],
                     "chair0"  : ["Une chaise, simple et solide"],
                     "nontendo": ["C'est la nouveauté incroyable de \nla compagnie de jeu vidéo \b(255,0,0)\bNontendo®","la \b(255,0,0)\bSUPER NONTENDO\b","Voulez-vous jouer ?",["Non","Non零","Non零零"]] }
 

import pygame as pg

class table_basse_rose :
    skn = pg.image.load('./img/obj/TV_stand_and_nontendo.png')
    def __init__(self,x,y):
        self.hb = [pg.Rect(x,y+55,167,95)]
        self.ib = [([pg.Rect(x+78,y+119,52,92)],["UP"],'textbox(settings["text"].observations["nontendo"])')]

class armoire_verte :
    skn = pg.image.load('./img/obj/locker.png')
    def __init__(self,x,y):
        self.hb = [pg.Rect(x,y,175,251)]
        self.ib = [([pg.Rect(x+48,y+218,80,80)],["UP"],'item_get("i000",5)')]

class lit_orange :
    skn = pg.image.load('./img/obj/bed.png')
    def __init__(self,x,y):
        self.hb = [pg.Rect(x,y,198,142)]
        self.ib = [([pg.Rect(x+19,y+111,160,64)],["UP"],"")]

class mur_de_ulo:
    skn = pg.image.load('./img/maps/mur_chambre_Ulo_bas.png')
    def __init__(self,x,y):
        self.hb = [pg.Rect(x,y+790,930,48)]
        self.ib = []

class table_avec_chaise:
    skn = pg.image.load('./img/obj/chair_and_table.png')
    def __init__(self,x,y):
        self.hb = [pg.Rect(x,y+40,187,115),pg.Rect(x+64,y+72,71,119+5)]
        self.ib = [([pg.Rect(x+64,y+8,64,64)],["DOWN"],'textbox(settings["text"].observations["anemone0"])')
                  ,([pg.Rect(x+136,y+148,24,56)],["LEFT"],'textbox(settings["text"].observations["chair0"])')
                  ,([pg.Rect(x+64,y+204,72,20)],["UP"],'textbox(settings["text"].observations["chair0"])')
                  ,([pg.Rect(x+40,y+148,24,56)],["RIGHT"],'textbox(settings["text"].observations["chair0"])')]

#class item id: #english name
#    type = usable/equipement/...
#    subtype = combatusable/weapon... (optional)
#    effect = ["stat/statuseffect",
#              option(if stat:modifier if status effect immunity/removal=0 apply=1),
#              length of the effect(0 for one time effects i.e. health potions etc)] ( optional )
#    worth = price when sold ( optional )
#    def __init__(self,options/targetstat...): if some effect is stat dependent i.e. restores half health...
#        self.effect += ...                     (item will have to be referenced with () at the end) ( optional )
class i000: #snail
    type = "usable"
    subtype = "combatusable"
    effect = [["hp",5,0]]
    img = "./img/non.png"
    worth = 100
    stack = True
